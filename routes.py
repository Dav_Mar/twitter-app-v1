from flask import Flask, render_template,request
from flask_restful import Resource, Api 
import datetime
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer,HashingVectorizer
from sklearn.model_selection import train_test_split
import scipy
from sklearn.svm import LinearSVC
import re
from bs4 import BeautifulSoup
import json
from pprint import pprint
import textblob as textblob
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from textblob import TextBlob
import csv
 
app = Flask(__name__)
api = Api(app)


#--------------------------------------------------------HOME--------------------------------------------  
@app.route('/')
def home():
  return render_template('home.html')

















#--------------------------------------------------------(1) CLEAN VARIABLES--------------------------------------------  

@app.route('/clean_variables')
def clean_variables():

      #EXTRACT DATE FOR TO CREATE UNIQUE FILE CSV
  now = datetime.datetime.now()
  try:
    d=str(now.microsecond*now.year/now.second+now.hour-now.minute*now.year)
  except:
    d=0
#CONFIGURATION
  conf=pd.read_csv('conf/conf_clean.csv',sep=',')
  var1=conf['VAR'][0]
  var2=conf['VAR'][1]
  test_file=conf['FILE_TEST'][0]
  output_file=conf['FILE_OUTPUT'][0]
  var1_output=conf['VAR_CLEANED'][0]
  var2_output=conf['VAR_CLEANED'][1]

#FUNCTION          
  def tweet_cleaner(text):     #clean word
    pat1 = r'@[A-Za-z0-9_]+'
    pat2 = r'https?://[^ ]+'
    combined_pat = r'|'.join((pat1, pat2))
    www_pat = r'www.[^ ]+'
    negations_dic = {"isn't":"is not", "aren't":"are not", "wasn't":"was not", "weren't":"were not",
                "haven't":"have not","hasn't":"has not","hadn't":"had not","won't":"will not",
                "wouldn't":"would not", "don't":"do not", "doesn't":"does not","didn't":"did not",
                "can't":"can not","couldn't":"could not","shouldn't":"should not","mightn't":"might not",
                "mustn't":"must not"}
    neg_pattern = re.compile(r'\b(' + '|'.join(negations_dic.keys()) + r')\b')
    soup=' '
    if text is None:
      text=u" "
    try:  
      soup=unicodedata.normalize('NFKD', text).encode('ascii','ignore')
    except:
      soup=soup
    soup = BeautifulSoup(text, 'lxml')
    souped = soup.get_text()
    try:
        bom_removed = souped.decode("utf-8-sig").replace(u"\ufffd", "a")
    except:
        bom_removed = souped
    stripped = re.sub(combined_pat, '', bom_removed)
    stripped = re.sub(www_pat, '', stripped)
    lower_case = stripped.lower()
    neg_handled = neg_pattern.sub(lambda x: negations_dic[x.group()], lower_case)
    letters_only = re.sub("[^a-zA-Z]", " ", neg_handled)
    words = [x for x  in str.split(str(letters_only)) if len(x) > 1]
    print('text cleaned')
    return (" ".join(words)).strip()
  

#IMPORT DATASETS WHERE f IS DATASET FOR TO TRAIN THE MODEL, INSTEAD y IS THE DATASET WHERE AGE IN UNKNOWN
  print('LOAD DATASET 1/4')
  y=pd.read_csv(test_file,sep=',')
  #COPY DATASET
  y[var1]=y[var1].fillna('unknown')
  y[var2]=y[var2].fillna('unknown')
  print('CLEAN VARIABLES 2/4')
  y[var1_output]=[tweet_cleaner(str(x)) for x in y[var1]]
  y[var2_output]=[tweet_cleaner(str(x)) for x in y[var2]]
  print('CREATE NEW VARIABLE text_desc 3/4')
  y['text_desc']=y[var1_output]+' '+y[var2_output]
  y=y.drop_duplicates()
  y.to_csv('result/'+output_file+str(d)+'.csv', sep=',', encoding='utf-8')
  print('COMPLETE 4/4')

  return ('Done! File result/'+output_file+str(d)+'.csv')






















#--------------------------------------------------------(2) EXTRACT DATA TWITTER API--------------------------------------------  

@app.route('/extract_tweets')
def extract_tweets():
  


  #CONFIGURATION
  conf=pd.read_csv('conf/conf_API.csv',sep=',')
  consumer_key=conf['consumer_key'][0]
  consumer_secret=conf['consumer_secret'][0]
  access_token=conf['access_token'][0]
  access_token_secret=conf['access_token_secret'][0]
  output_file='result/'+str(conf['OUTPUT_FILE'][0])
  filter_file='conf/'+str(conf['FILTER'][0])


 
  x = []

  with open(filter_file) as f:
     c = csv.reader(f, delimiter=',', skipinitialspace=True)
     for line in c:
        for i in range(0,2000):
            try:
              x.append(line[i])
            except:
              print('')


  class StdOutlistener(StreamListener):
      def on_error(self, status):
          print(status)

      def on_data(self, data):

          all_data = json.loads(data)
          tweet = all_data

          #Add the 'sentiment data to all_data
          #all_data['sentiment'] = tweet.sentiment

          for i in x:
              if ((i in str(tweet))==True):
                  with open(output_file, 'a') as tf:
                    # Write a new line

                      print('tweet saved')
                      tf.write('\n')


                    # Write the json data directly to the file
                      json.dump(all_data, tf)
                    # Alternatively: tf.write(json.dumps(all_data))
                  return True
                  break


        # Open json text file to save the tweets






  auth = OAuthHandler(consumer_key, consumer_secret)
  auth.set_access_token(access_token, access_token_secret)




  twitterStream = Stream(auth, StdOutlistener())
  twitterStream.filter(languages=["en"], track=x
)


  return ('Done! File result.csv')




















#--------------------------------------------------------(3) PREDICT AGE--------------------------------------------  

@app.route('/predict_age')
def predict_age():
  

      #EXTRACT DATE FOR TO CREATE UNIQUE FILE CSV
  now = datetime.datetime.now()
  try:
    d=str(now.microsecond*now.year/now.second+now.hour-now.minute*now.year)
  except:
    d=0

  #CONFIGURATION
  conf=pd.read_csv('conf/conf_predage.csv',sep=',')
  test_file=conf['FILE_TEST'][0]
  var1=conf['VAR'][0]
  var2=conf['VAR'][1]
  train_file=conf['FILE_TRAIN'][0]
  output_file=conf['FILE_OUTPUT'][0]
  #IMPORT DATASETS WHERE f IS DATASET FOR TO TRAIN THE MODEL, INSTEAD y IS THE DATASET WHERE AGE IN UNKNOWN
  print("LOAD DATASET 1/3")
  f=pd.read_csv(train_file,sep='\t')
  y=pd.read_csv(test_file,sep=',')
  #COPY DATASET
  y_original=y
  y=y[['text_desc']]
  y=y.fillna(' ')

  y['age']=['bo' for i in y['text_desc']]


  x=f[['text_desc','age']]

  frames = [x, y]

  result = pd.concat(frames)

  #BEST MODEL WITH TEXT
  print("VECTORIZER 2/3")

  text_train=result['text_desc']        


  tfidf_vectorizer = TfidfVectorizer(max_df=0.99, max_features=8000,
                                 min_df=0.000075,
                                 use_idf=False, ngram_range=(1,2))
  hash_vectorizer = HashingVectorizer()

  tfidf_matrix = tfidf_vectorizer.fit_transform(text_train)
  hash_matrix = hash_vectorizer.fit_transform(text_train)
  feature_names = tfidf_vectorizer.get_feature_names()

  X=tfidf_matrix[0:x.shape[0]]
  Y=tfidf_matrix[x.shape[0]:result.shape[0]]

  label_train=x['age']  

  X_train, X_test, y_train, y_test = train_test_split(X,label_train, test_size=0)

  label_train=y['age']  

  Y_star, Y_test, y2_train, y2_test = train_test_split(Y,label_train, test_size=0)

  print("MACHINE LEARNING WITH SVM LINEAR 3/3")
  clf_svm = LinearSVC(multi_class = 'crammer_singer')
  clf_svm.fit(X_train,y_train)
  pred_svm = clf_svm.predict(Y_star)
  y_original['age']=pred_svm
  y_original.to_csv('result/'+output_file+str(now)+'.csv', sep=',', encoding='utf-8')
  

  return ('Done! File result/'+output_file+str(now)+'.csv')

















#--------------------------------------------------------(4) JSON_TO_CSV--------------------------------------------  


@app.route('/JSON_to_CSV')
def JSON_to_CSV():
    #CONFIGURATION
   conf=pd.read_csv('conf/conf_jsoncsv.csv',sep=',')
   output_file=conf['CSV_OUTPUT'][0]
   input_file=conf['JSON_INPUT'][0]
      #EXTRACT DATE FOR TO CREATE UNIQUE FILE CSV
   now = datetime.datetime.now()
   try:
     d=str(now.microsecond*now.year/now.second+now.hour-now.minute*now.year)
   except:
     d=0
   
   def read_json_m(rows):    #read file json multiple
    k = ""
    for row in rows:
        k += row
        try:
            yield json.loads(k)
            k = ""
        except ValueError:
            pass
   #CREATE DATAFRAME FROM JSON FILE        
   print("OPEN JSON FILE 1/2")
   column = np.array([['','User Name','favourites_count','followers_count','friends_count','description','statuses_count','listed_count','text']])          
   f=pd.DataFrame(index=column[1:,0],columns=column[0,1:])

   i=0
   MB=0.00594924121233
   with open(input_file) as g:
     for parsed_json in read_json_m(g):
      #print (parsed_json)
      d=parsed_json  
      if (d['user']['description']!=None):
         k1=d['user']['description'].replace("\\n",' ').replace("\n",' ').rstrip()
         k1="\n".join(k1.splitlines())
      else:
         k1='unknown'
      if (d['text']!=None):
         k2=d['text'].replace("\\n",' ').replace("\n",' ').rstrip()
         k2="\n".join(k2.splitlines())
      else:
         k2='unknown'
      d1=np.array([d['user']['name'],
           d['user']['favourites_count'],
           d['user']['followers_count'],
           d['user']['friends_count'],  
          k1,
           d['user']['statuses_count'],
           d['user']['listed_count'],
           k2
          ])

      f.loc[i]=d1
      MB=MB+0.00594924121233
      print('MB: '+str(MB))
      i=i+1

   print("COMPLETE 2/2")
   f.to_csv('result/'+output_file+str(now)+'.csv', sep=',', encoding='utf-8')




   return('Done! File result/'+output_file+str(now)+'.csv')








#--------------------------------------------------------(5) PREDICT GENDER--------------------------------------------  

@app.route('/predict_gender')
def predict_gender():
  

      #EXTRACT DATE FOR TO CREATE UNIQUE FILE CSV
  now = datetime.datetime.now()
  try:
    d=str(now.microsecond*now.year/now.second+now.hour-now.minute*now.year)
  except:
    d=0

  #CONFIGURATION
  conf=pd.read_csv('conf/conf_predgender.csv',sep=',')
  input_file=conf['FILE_TEST'][0]
  var1=conf['VAR'][0]
  var2=conf['VAR'][1]
  var3=conf['VAR'][2]
  gender_train=conf['FILE_TRAIN'][0]
  gender_train2=conf['FILE_TRAIN'][1]
  output_file=conf['FILE_OUTPUT'][0]
  use_sx=conf['s_x'][0]



  #IMPORT DATASETS WHERE f IS DATASET FOR TO TRAIN THE MODEL, INSTEAD y IS THE DATASET WHERE AGE IN UNKNOWN
  def name_to_gender(name,d):    #convert name to gender
   if(len(name)<3):            
    name='aa'
   k=d[name[0:2]]
     
   t=k[['gender','name','probability']].loc[k['name'] == name] 

   if t.shape[0]==0: 
      t='unknown'
   else:    
      t=t.sort_values('probability')['gender'].iloc[0]
   return(t)   
  

  def dict(dataset,col='name2',p=2, number='no'):      #create a dictionary
  #dataset='name pandas dataset' , col='name of column', p=2/3 'number of permutations       
  #number='no/yes if include just alphabetic number or number too'            
    if (number=='yes'):
      a=['A', 'B', 'C', 'D', 'E','F','G','H', 'I','J','K','L','M', 'N', 'O', 'P' ,'Q','R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z','0','1','2','3','4','5','6','7','8','9']
    else:
      a=['A', 'B', 'C', 'D', 'E','F','G','H', 'I','J','K','L','M', 'N', 'O', 'P' ,'Q','R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z']
  
    k=[0]*len(a)**2
    t=0
    for j in a:
     for i in a:
          k[t]=j+i
          k[t]=k[t].lower()
          t=t+1
        
    if(p==3):     
      k2=[0]*len(k)**2
      t=0
      for j in k:
        for i in a:
           k2[t]=j+i
           k2[t].lower()
           t=t+1
        k=k2
    else:
         k=k
 
    c={}
    for i in k:
        t=dataset.loc[dataset[col] == i]
        c[i]=t
 
    return(c) 


  def tweet_cleaner(text):     #clean word
    pat1 = r'@[A-Za-z0-9_]+'
    pat2 = r'https?://[^ ]+'
    combined_pat = r'|'.join((pat1, pat2))
    www_pat = r'www.[^ ]+'
    negations_dic = {"isn't":"is not", "aren't":"are not", "wasn't":"was not", "weren't":"were not",
                "haven't":"have not","hasn't":"has not","hadn't":"had not","won't":"will not",
                "wouldn't":"would not", "don't":"do not", "doesn't":"does not","didn't":"did not",
                "can't":"can not","couldn't":"could not","shouldn't":"should not","mightn't":"might not",
                "mustn't":"must not"}
    neg_pattern = re.compile(r'\b(' + '|'.join(negations_dic.keys()) + r')\b')
    soup='a'
    if text is None:
      text=u" "
    try:  
      soup=unicodedata.normalize('NFKD', text).encode('ascii','ignore')
    except:
      soup=soup
    soup = BeautifulSoup(text, 'lxml')
    souped = soup.get_text()
    try:
        bom_removed = souped.decode("utf-8-sig").replace(u"\ufffd", "a")
    except:
        bom_removed = souped
    stripped = re.sub(combined_pat, '', bom_removed)
    stripped = re.sub(www_pat, '', stripped)
    lower_case = stripped.lower()
    neg_handled = neg_pattern.sub(lambda x: negations_dic[x.group()], lower_case)
    letters_only = re.sub("[^a-zA-Z]", " ", neg_handled)
    words = [x for x  in str.split(str(letters_only)) if len(x) > 1]
    return (" ".join(words)).strip()



  def splitter(a):
    a=str(a).split(' ')
    return(a[0])              #take just the first name
  
  def nametwo(a):
    a=a[0:2]
    return(a)                 #take the first two letters in the name


  def genderTOfloat(t):
    if (t=='M'):
        t=0.0
    elif (t=='F'):
        t=1.0
    elif (t=='B'):
        t=2.0
    else:
        t=t
    return (t)



  if (use_sx==True):
     print("LOAD DATAFRAMES 1/6")
     gender=pd.read_csv(gender_train,sep=',')
     df=pd.read_csv(input_file,sep=',')
     d=dict(dataset=gender, col='name2', p=2,number='no')
     print("CREATE DICTIONARY 2/6")


     df['filt']=[tweet_cleaner(str(x)) for x in df[var1]]

     df['FirstName']=[splitter(x) for x in df['filt']] 
     df['name2']=[nametwo(x) for x in df['FirstName']]

     df=df.drop([var1,'filt'],axis=1)

     print("USE s(x) FUNCTION 3/6")
     df['gender']=[name_to_gender(x,d) for x in df['FirstName']]

     y_1=df.where(df['gender']!='unknown').dropna(subset=['gender'])     #without NaN
     y=df.where(df['gender']=='unknown').dropna(subset=['gender'])
     y_2=y
     y=y[['text_desc','gender']]
  else:
     print("LOAD DATAFRAMES 1/6")
     y=pd.read_csv(input_file,sep=',')
     y_2=y
     y['gender']='Bo'
     y=y[['text_desc','gender']]




  
  f=pd.read_csv(gender_train2,sep=',')
  x=f[['text_desc','gender']]

  y=pd.read_csv(input_file,sep=',')
  y_2=y
  y['gender']='Bo'
  y=y[['text_desc','gender']]

  
  f=pd.read_csv(gender_train2,sep=',')
  x=f[['text_desc','gender']]
  
  frames = [x, y]

  result = pd.concat(frames)
  
  result['text_desc']=result.fillna(' ')
  result.index = pd.RangeIndex(len(result.index))

  result.index = range(len(result.index))


  frames = [x, y]

  result = pd.concat(frames)
  
  result['text_desc']=result.fillna(' ')
  result.index = pd.RangeIndex(len(result.index))

  result.index = range(len(result.index))
  
  
  #BEST MODEL WITH TEXT

  print("VECTORIZER 4/6")
  text_train=result['text_desc']        


  tfidf_vectorizer = TfidfVectorizer(max_df=0.3, max_features=9000,min_df=0.00025,use_idf=True, ngram_range=(1,3),stop_words = 'english')
  hash_vectorizer = HashingVectorizer()

  tfidf_matrix = tfidf_vectorizer.fit_transform(text_train)
  hash_matrix = hash_vectorizer.fit_transform(text_train)
  feature_names = tfidf_vectorizer.get_feature_names()

  X=tfidf_matrix[0:x.shape[0]]
  Y=tfidf_matrix[x.shape[0]:result.shape[0]]

  label_train=x['gender']  

  X_train, X_test, y_train, y_test = train_test_split(X,label_train, test_size=0)

  y_train=[genderTOfloat(x) for x in y_train]

  label_train=y['gender']  

  Y_star, Y_test, y2_train, y2_test = train_test_split(Y,label_train, test_size=0)
  print("MACHINE LEARNING SVM LINEAR 5/6")
  clf_svm = LinearSVC(multi_class = 'crammer_singer')
  clf_svm.fit(X_train,y_train)
  pred_svm = clf_svm.predict(Y_star)


  if (use_sx==True):
     y_2['gender']=pred_svm
     frames = [y_1, y_2]
     r = pd.concat(frames) 
     r['gender']=[genderTOfloat(x) for x in r['gender']]
     r.to_csv('result/'+output_file+str(now)+'.csv', sep=',', encoding='utf-8')
  else:
     y_2['gender']=[genderTOfloat(x) for x in pred_svm]
     y_2.to_csv('result/'+output_file+str(now)+'.csv', sep=',', encoding='utf-8')
  
  print("COMPLETE 6/6")


  return ('Done! File result/'+output_file+str(now)+'.csv')












#--------------------------------------------------------(6) PREDICT GENDER AND AGE--------------------------------------------  

@app.route('/predict_gender_age')
def predict_gender_age():
  

      #EXTRACT DATE FOR TO CREATE UNIQUE FILE CSV
  now = datetime.datetime.now()
  try:
    d=str(now.microsecond*now.year/now.second+now.hour-now.minute*now.year)
  except:
    d=0

  #CONFIGURATION
  conf=pd.read_csv('conf/conf_pred_gender_age.csv',sep=',')
  input_file=conf['FILE_TEST'][0]
  var1=conf['VAR'][0]
  var2=conf['VAR'][1]
  var3=conf['VAR'][2]
  gender_train=conf['FILE_TRAIN'][0]
  gender_train2=conf['FILE_TRAIN'][1]
  trainfile_age=conf['FILE_TRAIN'][2]
  output_file=conf['FILE_OUTPUT'][0]
  use_sx=conf['s_x'][0]



  #IMPORT DATASETS WHERE f IS DATASET FOR TO TRAIN THE MODEL, INSTEAD y IS THE DATASET WHERE AGE IN UNKNOWN
  def name_to_gender(name,d):    #convert name to gender
   if(len(name)<3):            
    name='aa'
   k=d[name[0:2]]
     
   t=k[['gender','name','probability']].loc[k['name'] == name] 

   if t.shape[0]==0: 
      t='unknown'
   else:    
      t=t.sort_values('probability')['gender'].iloc[0]
   return(t)   
  

  def dict(dataset,col='name2',p=2, number='no'):      #create a dictionary
  #dataset='name pandas dataset' , col='name of column', p=2/3 'number of permutations       
  #number='no/yes if include just alphabetic number or number too'            
    if (number=='yes'):
      a=['A', 'B', 'C', 'D', 'E','F','G','H', 'I','J','K','L','M', 'N', 'O', 'P' ,'Q','R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z','0','1','2','3','4','5','6','7','8','9']
    else:
      a=['A', 'B', 'C', 'D', 'E','F','G','H', 'I','J','K','L','M', 'N', 'O', 'P' ,'Q','R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z']
  
    k=[0]*len(a)**2
    t=0
    for j in a:
     for i in a:
          k[t]=j+i
          k[t]=k[t].lower()
          t=t+1
        
    if(p==3):     
      k2=[0]*len(k)**2
      t=0
      for j in k:
        for i in a:
           k2[t]=j+i
           k2[t].lower()
           t=t+1
        k=k2
    else:
         k=k
 
    c={}
    for i in k:
        t=dataset.loc[dataset[col] == i]
        c[i]=t
 
    return(c) 


  def tweet_cleaner(text):     #clean word
    pat1 = r'@[A-Za-z0-9_]+'
    pat2 = r'https?://[^ ]+'
    combined_pat = r'|'.join((pat1, pat2))
    www_pat = r'www.[^ ]+'
    negations_dic = {"isn't":"is not", "aren't":"are not", "wasn't":"was not", "weren't":"were not",
                "haven't":"have not","hasn't":"has not","hadn't":"had not","won't":"will not",
                "wouldn't":"would not", "don't":"do not", "doesn't":"does not","didn't":"did not",
                "can't":"can not","couldn't":"could not","shouldn't":"should not","mightn't":"might not",
                "mustn't":"must not"}
    neg_pattern = re.compile(r'\b(' + '|'.join(negations_dic.keys()) + r')\b')
    soup='a'
    if text is None:
      text=u" "
    try:  
      soup=unicodedata.normalize('NFKD', text).encode('ascii','ignore')
    except:
      soup=soup
    soup = BeautifulSoup(text, 'lxml')
    souped = soup.get_text()
    try:
        bom_removed = souped.decode("utf-8-sig").replace(u"\ufffd", "a")
    except:
        bom_removed = souped
    stripped = re.sub(combined_pat, '', bom_removed)
    stripped = re.sub(www_pat, '', stripped)
    lower_case = stripped.lower()
    neg_handled = neg_pattern.sub(lambda x: negations_dic[x.group()], lower_case)
    letters_only = re.sub("[^a-zA-Z]", " ", neg_handled)
    words = [x for x  in str.split(str(letters_only)) if len(x) > 1]
    return (" ".join(words)).strip()



  def splitter(a):
    a=str(a).split(' ')
    return(a[0])              #take just the first name
  
  def nametwo(a):
    a=a[0:2]
    return(a)                 #take the first two letters in the name


  def genderTOfloat(t):
    if (t=='M'):
        t=0.0
    elif (t=='F'):
        t=1.0
    elif (t=='B'):
        t=2.0
    else:
        t=t
    return (t)



  if (use_sx==True):
     print("LOAD DATAFRAMES 1/8")
     gender=pd.read_csv(gender_train,sep=',')
     df=pd.read_csv(input_file,sep=',')
     d=dict(dataset=gender, col='name2', p=2,number='no')
     print("PREDICT GENDER: CREATE DICTIONARY 2/8")


     df['filt']=[tweet_cleaner(str(x)) for x in df[var1]]

     df['FirstName']=[splitter(x) for x in df['filt']] 
     df['name2']=[nametwo(x) for x in df['FirstName']]

     df=df.drop([var1,'filt'],axis=1)

     print("PREDICT GENDER: USE s(x) FUNCTION 3/8")
     df['gender']=[name_to_gender(x,d) for x in df['FirstName']]

     y_1=df.where(df['gender']!='unknown').dropna(subset=['gender'])     #without NaN
     y=df.where(df['gender']=='unknown').dropna(subset=['gender'])
     y_2=y
     y=y[['text_desc','gender']]

  else:
     print("LOAD DATAFRAMES 1/8")
     y=pd.read_csv(input_file,sep=',')
     y_2=y
     y['gender']='Bo'
     y=y[['text_desc','gender']]




  
  f=pd.read_csv(gender_train2,sep=',')
  x=f[['text_desc','gender']]

  y=pd.read_csv(input_file,sep=',')
  y_2=y
  y['gender']='Bo'
  y=y[['text_desc','gender']]

  
  f=pd.read_csv(gender_train2,sep=',')
  x=f[['text_desc','gender']]
  
  frames = [x, y]

  result = pd.concat(frames)
  
  result['text_desc']=result.fillna(' ')
  result.index = pd.RangeIndex(len(result.index))

  result.index = range(len(result.index))


  frames = [x, y]

  result = pd.concat(frames)
  
  result['text_desc']=result.fillna(' ')
  result.index = pd.RangeIndex(len(result.index))

  result.index = range(len(result.index))
  
  
  #BEST MODEL WITH TEXT

  print("PREDICT GENDER: VECTORIZER 4/8")
  text_train=result['text_desc']        


  tfidf_vectorizer = TfidfVectorizer(max_df=0.3, max_features=9000,min_df=0.00025,use_idf=True, ngram_range=(1,3),stop_words = 'english')
  hash_vectorizer = HashingVectorizer()

  tfidf_matrix = tfidf_vectorizer.fit_transform(text_train)
  hash_matrix = hash_vectorizer.fit_transform(text_train)
  feature_names = tfidf_vectorizer.get_feature_names()

  X=tfidf_matrix[0:x.shape[0]]
  Y=tfidf_matrix[x.shape[0]:result.shape[0]]

  label_train=x['gender']  

  X_train, X_test, y_train, y_test = train_test_split(X,label_train, test_size=0)

  y_train=[genderTOfloat(x) for x in y_train]

  label_train=y['gender']  

  Y_star, Y_test, y2_train, y2_test = train_test_split(Y,label_train, test_size=0)
  print("PREDICT GENDER: MACHINE LEARNING SVM LINEAR 5/8")
  clf_svm = LinearSVC(multi_class = 'crammer_singer')
  clf_svm.fit(X_train,y_train)
  pred_svm = clf_svm.predict(Y_star)



  if (use_sx==True):
     y_2['gender']=pred_svm
     frames = [y_1, y_2]
     y_2 = pd.concat(frames) 
     y_2['gender']=[genderTOfloat(x) for x in y_2['gender']]
  else:
     y_2['gender']=[genderTOfloat(x) for x in pred_svm]
     y_2.to_csv('result/'+output_file+str(now)+'.csv', sep=',', encoding='utf-8')

 #COPY DATASET
  f=pd.read_csv(trainfile_age,sep='\t')
  y=y_2
  #COPY DATASET
  y_original=y
  y=y[['text_desc']]
  y=y.fillna(' ')

  y['age']=['bo' for i in y['text_desc']]


  x=f[['text_desc','age']]
  frames = [x, y]

  result = pd.concat(frames)


  #BEST MODEL WITH TEXT
  print("PREDICT AGE: VECTORIZER 6/8")

  text_train=result['text_desc']        


  tfidf_vectorizer = TfidfVectorizer(max_df=0.99, max_features=8000,
                                 min_df=0.000075,
                                 use_idf=False, ngram_range=(1,2))
  hash_vectorizer = HashingVectorizer()

  tfidf_matrix = tfidf_vectorizer.fit_transform(text_train)
  hash_matrix = hash_vectorizer.fit_transform(text_train)
  feature_names = tfidf_vectorizer.get_feature_names()

  X=tfidf_matrix[0:x.shape[0]]
  Y=tfidf_matrix[x.shape[0]:result.shape[0]]

  label_train=x['age']  

  X_train, X_test, y_train, y_test = train_test_split(X,label_train, test_size=0)

  label_train=y['age']  

  Y_star, Y_test, y2_train, y2_test = train_test_split(Y,label_train, test_size=0)

  print("PREDICT AGE: MACHINE LEARNING WITH SVM LINEAR 7/8")
  clf_svm = LinearSVC(multi_class = 'crammer_singer')
  clf_svm.fit(X_train,y_train)
  pred_svm = clf_svm.predict(Y_star)
  y_original['age']=pred_svm
  y_original.to_csv('result/'+output_file+str(now)+'.csv', sep=',', encoding='utf-8')

  print("COMPLETE 8/8")
  return ('Done! File result/'+output_file+str(now)+'.csv')


















  
if __name__ == '__main__':
  app.run(debug=True)